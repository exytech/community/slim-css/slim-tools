var fs = require('fs');
var path = require('path');
var mkdirp = require('mkdirp');


var config_filename = '.slimrc.js'
var base_dir = path.dirname(path.dirname(path.dirname(path.dirname(__dirname))))
var config_filepath = path.join(base_dir, config_filename)

if (!fs.existsSync(config_filepath)) {
    throw new Error(`${config_filename} not found.`)
}

var configs = require(config_filepath)

configs.source = path.join(base_dir, configs.source);
configs.output = path.join(base_dir, configs.output);

mkdirp(configs.source)
mkdirp(configs.output)

var configstr_keys = [
    'pluginName',
    'source',
    'output',
]

if (typeof configs['entryFilename'] === 'string')
    configstr_keys.unshift('entryFilename')
else if (configs.pluginName !== 'slim-core') {
    throw new Error('entryFile must be string type.')
} else {
    for (const key in configs.entryFilename) {
        if (Object.hasOwnProperty.call(configs.entryFilename, key)) {
            const scss_file = path.join(configs.source, configs.entryFilename[key] + '.scss');
            const css_file = path.join(configs.output, configs.entryFilename[key] + '.css');
            fs.appendFile(scss_file, '', function (err) {
                if (err) throw err;
            })
            fs.appendFile(css_file, '', function (err) {
                if (err) throw err;
            })
        }
    }
}

for (const key of configstr_keys) {
    if (!configs[key])
        throw new Error(`'${key}' not found in ${config_filepath}`)
    else if ((key === 'source' || key === 'output') && typeof configs.entryFilename === 'string') {
        var file = configs.entryFilename + (key === 'source' ? '.scss': '.css');
        var file_dir = path.join(configs[key], file)
        if (!fs.existsSync(file_dir)) {
            fs.appendFile(file_dir, '', function (err) {
                if (err) throw err;
            })
        }
    }
}

module.exports = configs;
