# Slim-scss-tools

slim-scss-tools is a great tool that provides you with a set of scss files (containing scss/css variables, mixins, functions, and extensions used by the components and elements that make up the slim-css core) and a command line tool to easily build and develop plugins and graphic designs for the slim-css framework.

## Table of contents
-  [Quick install](#quick-install)
-  [Import slim-scss-tools](#import-slim-scss-tools)
-  [Use slim-scss-cli](#use-slim-scss-cli)
-  [Copyright and license](#copyright-and-license)

## Quick install
### NPM
```bash
npm i -D @slim-css/scss-tools
```

***or***

### Yarn
```bash
yarn add -D @slim-css/scss-tools
```

## Import slim-scss-tools
In your own scss file (example.scss) you can include the scss tools from slim-core as follows

```scss
// ./scss/example.scss

@import  "../node_modules/@slim-css/scss-tools/src/tools_components/all";
```

## Use slim-scss-cli
### Create .slimrc.js
To use slim-scss-cli, you must first create a configuration file named .slimrc.js whose content will look like the following
```js
// .slimrc.js

module.exports = {
    pluginName: 'plugin_name', // A plugin name
    source: './scss', // the source directory path that contains the scss files
    output: './css', // the output directory path that will contain the css files
    entryFilename: 'plugin_name', // name of the scss input file that will be compiled
}
```

### Set package
Add to your package.json the following lines of code
```json
"scripts": {
	"build": "slim-cli sass && slim-cli group && slim-cli prefix && slim-cli cleancss",
	"dev": "npm run sass -- --watch",
	"prod": "slim-cli clean && npm run build"
}
```

> - **dev**: compile at each modification your scss entrypoint in compact css with map file 
> - **build**: compile at each modification your scss entrypoint in minified css
> - **prod**: clean up the css output folder before building the scss entry point (very useful for the plugin production)

### Command line reference
|Commands        |Functions                          	 |
|----------------|---------------------------------------|
|clean			 |`Clean css folder`            		 |
|sass          	 |`Run sass compiler`            		 |
|cleancss        |`Remove css files in css folder`		 |
|group           |`Group css media querie`            	 |
|prefix          |`Auto prefix css attribute`            |


## Copyright and license

Code copyright 2021 SOCGNA KOUYEM Childéric. Code released under [the MIT license](https://gitlab.com/exytech/community/slim-css/slim-tools/-/blob/main/LICENSE).
