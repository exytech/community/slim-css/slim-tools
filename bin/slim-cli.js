#!/usr/bin/env node

var { exec, spawn } = require("child_process");
var path = require('path');
var yargs = require('yargs');

var configs = require('../src/configs');

var argv = yargs
    .command('clean', 'Clean css folder')
    .command('sass', 'Run sass compiler')
    .command('cleancss', 'Clean css files')
    .command('group', 'Group css media querie')
    .command('prefix', 'Auto prefix css attribute')
    .help()
    .alias('help', 'h')
    .argv;

function runCommand(command) {
    exec(`node ./node_modules/.bin/${command}`,  (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(stdout);
    })
}

function minified_file(css_file) {
    return css_file.replace('.css', '.min.css')
}

function runGroupCommand(command) {
    var element = configs.entryFilename;
    var output_file = path.join(configs.output, element + '.css')

    if (configs.pluginName !== 'slim-core') {
        runCommand(command !== 'cleancss' ? `${command} ${output_file} ${output_file}` : `cleancss -o ${minified_file(output_file)} ${output_file}`)
    } else {
        for (var key in configs.entryFilename) {
            if (Object.hasOwnProperty.call(configs.entryFilename, key)) {
                var element = configs.entryFilename[key];
                output_file = path.join(configs.output, element + '.css')
                runCommand(command !== 'cleancss' ? `${command} ${output_file} ${output_file}` : `cleancss -o ${minified_file(output_file)} ${output_file}`)
            }
        }
    }
}

function slimCli() {
    if (argv._.length === 1) {
        switch (argv._[0]) {
            case 'clean':
                runCommand(`rimraf ${configs.output}`)
                break;
            case 'sass':
                runCommand(`node-sass -o ${configs.output} ${configs.source} --output-style expanded --source-map true`)
                break;
            case 'cleancss':
                runGroupCommand('cleancss')
                break;
            case 'group':
                runGroupCommand('group-css-media-queries')
                break;
            case 'prefix':
                runGroupCommand('postcss --use autoprefixer --map false --output')
                break;
            default:
                throw new Error('Unrecognize arg');
        }
    }  else {
        console.error("Overflow args")
    }    
}

slimCli()
